from enum import auto, Enum


class RenderOrder(Enum):
    CORPS = auto()
    ITEM = auto()
    ACTOR = auto()
