class Impossible(Exception):
    """Exception raised when an action is impossible to be perfromed.

    The reason is given as the exception message.
    """


class QuitWithoutSaving(SystemExit):
    """Can be raise to exit the game without automatically saving."""
